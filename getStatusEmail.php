<?php
//Get Request untuk dari JSON rubah dahulu, disini tidak perlu api Key Dll karena takde auth
$request = (array) json_decode(file_get_contents('php://input'), TRUE);
//Function untuk Check email Domain apakah sudah terdaftar atau disebut dengan Mx Record.
function mxrecordValidate($email, $domain){
    $arr = dns_get_record($domain, DNS_MX);
    if(isset($arr[0]) && $arr[0]['host'] == $domain && !empty($arr[0]['target'])){
        return $arr[0]["target"];
    }
	
}
// Check GET Request sudah benar atau belum
if(isset($request["resource"])){
    //Kita pecah dalam bentuk Array
	foreach($request["resource"] as $k=>$item){
        //identifikasi Email
        $email =$request["resource"][$k]["email"];
        //pecah domain dan email name
        $domain = substr(strrchr($email, "@"),1);
        //proses apakah email Valid atau Invalid dengan menjalankan fungsi mxrecordValidate.
		if(mxrecordValidate($email, $domain)){
            //check dns Record
            $data = dns_get_record($domain, DNS_MX);
            //masukan hasil ke Response
		    foreach($data as $key1){
		        $response["resource"][$k]["host"]=$key1['host'] ;
                $response["resource"][$k]["class"]=$key1['class'] ;
                $response["resource"][$k]["ttl"]=$key1['ttl'] ;
                $response["resource"][$k]["type"]=$key1['type'] ;
                $response["resource"][$k]["pri"]=$key1['pri'] ;
                $response["resource"][$k]["target"]=$key1['target'] ;
                $response["resource"][$k]["target"]=gethostbyname($key1['target']) ;
            }
            //Status untuk Valid
			$response["resource"][$k]["email"] = $request["resource"][$k]["email"];
			$response["resource"][$k]["status"] = 'Valid Email';
		} else {
            //Status untuk Invalid
			$response["resource"][$k]["email"] = $request["resource"][$k]["email"];
			$response["resource"][$k]["status"] = 'Invalid Email';
		}
	}
} else {
    //Berikan Error untuk orang yang input sembarangan, dengan status 500 Internal Server Error.
    header('HTTP/1.1 500 Internal Server Error');
    //Response untuk Error.
	$response["message"] = "Error fix your Brain.";  
}

header('Content-Type: application/json');
$json_string = json_encode($response, JSON_PRETTY_PRINT);
die($json_string);
?>